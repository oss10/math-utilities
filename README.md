# Math Utilities

Some useful math utilities

![CI/CD](https://gitlab.com/oss10/nodejs-template/badges/master/pipeline.svg)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)
<br />
[![codecov](https://codecov.io/gl/oss10/math-utilities/branch/master/graph/badge.svg?token=GQSPVJDJ48)](https://codecov.io/gl/oss10/math-utilities)

## Features

- Release GitLab with `release-it`.
- Publish package to NPM
