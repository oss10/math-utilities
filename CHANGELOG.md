# 1.0.0 (2020-12-10)


### Bug Fixes

* fix codecov scripts ([06865ba](https://gitlab.com/oss10/math-utilities/commit/06865baddc2ec30a2fe6caf7217e25a12e0b8996))


### Features

* update initial source code ([499c66f](https://gitlab.com/oss10/math-utilities/commit/499c66fd80241c0bc5769e27580e6c85b2b47257))

